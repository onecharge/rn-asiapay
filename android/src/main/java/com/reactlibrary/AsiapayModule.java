package com.reactlibrary;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.Callback;
import com.asiapay.sdk.PaySDK;
import com.asiapay.sdk.integration.CardDetails;
import com.asiapay.sdk.integration.Data;
import com.asiapay.sdk.integration.EnvBase;
import com.asiapay.sdk.integration.PayData;
import com.asiapay.sdk.integration.PayMethodResponse;
import com.asiapay.sdk.integration.PayMethodResult;
import com.asiapay.sdk.integration.PayResult;
import com.asiapay.sdk.integration.PaymentResponse;
import com.asiapay.sdk.integration.QueryResponse;
import com.asiapay.sdk.integration.TransactionStatus;
import com.asiapay.sdk.integration.googlepay.GooglePay;
import com.asiapay.sdk.integration.googlepay.PaymentsUtil;
import com.asiapay.sdk.integration.xecure3ds.ThreeDSParams;
import com.asiapay.sdk.integration.xecure3ds.spec.ConfigParameters;
import com.asiapay.sdk.integration.xecure3ds.spec.Factory;
import com.asiapay.sdk.integration.xecure3ds.spec.UiCustomization;

import com.asiapay.sdk.PaySDK;
import com.asiapay.sdk.integration.CardDetails;
import com.asiapay.sdk.integration.Data;
import com.asiapay.sdk.integration.EnvBase;
import com.asiapay.sdk.integration.PayData;
import com.asiapay.sdk.integration.PayMethodResponse;
import com.asiapay.sdk.integration.PayMethodResult;
import com.asiapay.sdk.integration.PayResult;
import com.asiapay.sdk.integration.PaymentResponse;
import com.asiapay.sdk.integration.QueryResponse;
import com.asiapay.sdk.integration.TransactionStatus;
import com.asiapay.sdk.integration.googlepay.GooglePay;
import com.asiapay.sdk.integration.googlepay.PaymentsUtil;
import com.asiapay.sdk.integration.xecure3ds.ThreeDSParams;
import com.asiapay.sdk.integration.xecure3ds.spec.ConfigParameters;
import com.asiapay.sdk.integration.xecure3ds.spec.Factory;
import com.asiapay.sdk.integration.xecure3ds.spec.UiCustomization;

import android.app.Activity;
import android.util.Log;
import android.net.Uri;
import android.content.Intent;

public class AsiapayModule extends ReactContextBaseJavaModule {

    private final ReactApplicationContext reactContext;
    private PaySDK paySDK;

    public AsiapayModule(ReactApplicationContext reactContext) {
        super(reactContext);
        paySDK = new PaySDK(reactContext);
        this.reactContext = reactContext;
    }

    @Override
    public String getName() {
        return "Asiapay";
    }

    @ReactMethod
    public void sampleMethod(String stringArgument, int numberArgument, Callback callback) {
        // TODO: Implement some actually useful functionality
        callback.invoke("Received numberArgument: " + numberArgument + " stringArgument: " + stringArgument);
    }

    // Only Alipay , wechat pay and octupus


    @ReactMethod
    public void aliPay(String amount, String orderRef, String merchantId, String remark) {
        paySDK = new PaySDK(reactContext);
        PayData payData = new PayData();
        payData.setChannel(EnvBase.PayChannel.DIRECT);
        payData.setEnvType(EnvBase.EnvType.SANDBOX);
        payData.setAmount(amount);
        payData.setPayGate(EnvBase.PayGate.PAYDOLLAR);
        payData.setCurrCode(EnvBase.Currency.HKD);
        payData.setPayType(EnvBase.PayType.NORMAL_PAYMENT);
        payData.setOrderRef(orderRef);
        //payData.setPayMethod(paymentMethod);
        payData.setPayMethod("ALIPAYHKAPP"); // FOR ALIPAY HK

        payData.setLang(EnvBase.Language.ENGLISH);
        payData.setMerchantId(merchantId);
        payData.setRemark(remark);
        payData.setActivity(getCurrentActivity());
        paySDK.setRequestData(payData);

        paySDK.process();
        paySDK.responseHandler(new PaymentResponse() {
            @Override
            public void getResponse(PayResult payResult) {
                Log.d("TAG","payResult: "+payResult.getSuccessMsg());
            }

            @Override
            public void onError(Data data) {
                Log.d("TAG","error returned : "+ data.getError());
            }
        });
    }

    @ReactMethod
    public void wechat(String amount,String orderRef, String payMethod, String merchantId, String remark) {
       // Toast.makeText(this, "okay", Toast.LENGTH_SHORT).show();
        PaySDK paySDK = new PaySDK(reactContext);
        PayData payData = new PayData();
        payData.setChannel(EnvBase.PayChannel.DIRECT);
        payData.setEnvType(EnvBase.EnvType.SANDBOX);
        payData.setAmount(amount);
        payData.setPayGate(EnvBase.PayGate.PAYDOLLAR);
        payData.setCurrCode(EnvBase.Currency.HKD);
        payData.setPayType(EnvBase.PayType.NORMAL_PAYMENT);
        payData.setActivity(getCurrentActivity());
        payData.setOrderRef(orderRef);
        payData.setPayMethod(payMethod);
        payData.setLang(EnvBase.Language.ENGLISH);
        payData.setMerchantId(merchantId);
        payData.setRemark(remark);
        paySDK.setRequestData(payData);

        paySDK.process();

        paySDK.responseHandler(new PaymentResponse() {
            @Override
            public void getResponse(PayResult payResult) {
                //payment Result
            }

            @Override
            public void onError(Data data) {
                //errror
            }
        });
    }

 /*   @ReactMethod
    public void octupus(String amount, String merchantId, String orderRef, String remark) {
        PayData payData = new PayData();
        payData = new PayData();
        payData.setChannel(EnvBase.PayChannel.DIRECT);
        payData.setEnvType(EnvBase.EnvType.SANDBOX);
        payData.setPayGate(EnvBase.PayGate.PAYDOLLAR);
        payData.setCurrCode(EnvBase.Currency.HKD);
        payData.setPayType(EnvBase.PayType.NORMAL_PAYMENT);
        payData.setLang(EnvBase.Language.ENGLISH);
        payData.setAmount(textAmount.getEditText().getText().toString());
        payData.setPayMethod("OCTOPUS");
        payData.setMerchantId(textMerchantId.getEditText().getText().toString());
        payData.setOrderRef(getOrderRef());
        payData.setRemark("test remark");
        payData.setActivity(reactContext);

        paySDK.setRequestData(payData);
        paySDK.process();

        paySDK.responseHandler(new PaymentResponse() {
            @Override
            public void getResponse(PayResult payResult) {

                cancelProgressDialog();

                try {

                    // method to get the URI
                    String octopusuri = paySDK.decodeData(payResult.getErrMsg());

                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri
                            .parse(octopusuri));
                    startActivityForResult(intent, OCTOPUS_APP_REQUEST_CODE);

                }catch (Exception e){

                    Log.d(TAG, "octopus: exp "+e.getMessage());
                }

            }

            @Override
            public void onError(Data data) {

                cancelProgressDialog();
                //showAlert(data.getMessage()+data.getError());
            }
        });
    }*/

}
