/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import Asiapay from 'react-native-Asiapay';
import {
    StyleSheet,
    Button,
    View,
    Alert} from 'react-native';

const App: () => React$Node = () => {
  return (
    <>
            <View style={styles.button}>
               <Button
                     title="Press me"
                     color="#f194ff"
                     onPress={() => Asiapay.aliPay()}/>
            </View>
    </>
  );
};


const styles = StyleSheet.create({
  button: {
    fontSize: 30,
  },
});

export default App;
