# react-native-asiapay

## Getting started

`$ npm install react-native-asiapay --save`

### Mostly automatic installation

`$ react-native link react-native-asiapay`

### Manual installation


#### iOS

1. In XCode, in the project navigator, right click `Libraries` ➜ `Add Files to [your project's name]`
2. Go to `node_modules` ➜ `react-native-asiapay` and add `Asiapay.xcodeproj`
3. In XCode, in the project navigator, select your project. Add `libAsiapay.a` to your project's `Build Phases` ➜ `Link Binary With Libraries`
4. Run your project (`Cmd+R`)<

#### Android

1. Open up `android/app/src/main/java/[...]/MainApplication.java`
  - Add `import com.reactlibrary.AsiapayPackage;` to the imports at the top of the file
  - Add `new AsiapayPackage()` to the list returned by the `getPackages()` method
2. Append the following lines to `android/settings.gradle`:
  	```
  	include ':react-native-asiapay'
  	project(':react-native-asiapay').projectDir = new File(rootProject.projectDir, 	'../node_modules/react-native-asiapay/android')
  	```
3. Insert the following lines inside the dependencies block in `android/app/build.gradle`:
  	```
      compile project(':react-native-asiapay')
  	```


## Usage
```javascript
import Asiapay from 'react-native-asiapay';

// TODO: What to do with the module?
Asiapay;

## AsiaPay testing
 1.Apply for a Merchant Account
 2.Integrate PayDollar payment services
 3. Application test with testing account


 ##parameters :                https://www.paydollar.com/pdf/op/enpdintguide.pdf
                               pg-19 for more detail

 ~~orderRef : Merchant's Order Reference Number
 ~~currCode : The currency of the payment
 ~~amount   : The total amount we want to charge the customer for the provided currency
 ~~lang     : The language of the payment page
 ~~cancelUrl: A Webpage address we want to redirect the customer upon the transaction being cancelled(for display purpose only)
 ~~failUrl  : A Webpage address we want to redirect the customer upon the transaction being rejected(for display purpose only)
 ~~successUrl: A Webpage address we want to redirect the customer upon the transaction being success(for display purpose only)
 ~~merchantId: The merchant ID we have
 ~~payType  : The payment type:
                "N" - Normal Payment(Sales)
                "H" – Hold Payment (Authorize only)
                *Remark: Hold Payment is not available for 99BILL, ALIPAY, CHIANPAY,
                 PAYPAL, PPS, TENPAY, WECHAT, MEPS,OCTOPUS
 ~~payMethod : The payment method:
               "ALL" – All the available payment method
               "CC" – Credit Card Payment
               "VISA" – Visa Payment
               "Master" – MasterCard Payment
               "JCB" – JCB Payment
               "AMEX" – AMEX Payment
               "Diners" – Diners Club Payment
               "PPS" – PayDollar PPS Payment
               "PAYPAL" – PayPal By PayDollar Payment
               "CHINAPAY" – China UnionPay By PayDollar Payment
               "ALIPAY" – ALIPAY By PayDollar Payment
               "TENPAY" – TENPAY BY PayDollar Payment
               "99BILL" – 99BILL BY PayDollar Payment
               "MEPS" – MEPS BY PayDollar Payment
               "SCB" –SCB (SCB Easy) BY PayDollar Payment
               "BPM" –Bill Payment BY PayDollar Payment
               "KTB" –Krung thai Bank (KTB Online) BY PayDollar Payment
               "UOB" –United Oversea bank BY PayDollar Payment
               "KRUNGSRIONLINE" –Bank of Ayudhya (KRUNGSRIONLINE) BY PayDollar Payment
               "TMB" –TMB Bank BY PayDollar Payment
               "IBANKING" –Bangkok Bank iBanking BY PayDollar Payment
               "UPOP" – UPOP BY PayDollar Payment
               "M2U" – M2U BY PayDollar Payment
               "CIMBCLICK" – CIMBCLICK BY PayDollar Payment
               "OCTOPUS" – OCTOPUS BY PayDollar Payment
               "WECHAT" – WECHAT BY PayDollar Payment
               "ONEPAY" – ONEPAY BY PayDollar Payment
               "VCO" – VISA Checkout Payment
               "MP" – MasterPass Payment
               "WELEND" – WELEND BY PayDollar Payment
               "MOMOPAY" – MoMo eWallet Payment
               "SAMSUNG" – Samsung Pay Payment

  ~~ remark : A remark field to store additional data that will not show on the
              transaction web page
```
